grav = 0.2;
hsp = 0;
vsp = 0;

movespeed = 2;

platform = false;

jumpspeed_normal = 4;
jumpspeed_powerup = 10;

jumpspeed = jumpspeed_normal;

//IA Checks
hcollision = false;
fearofheights = false;

//Constants
grounded = false;
jumping = false;
isShoot = false;

//Horizontal jump constants
hsp_jump_constant_small = 1;
hsp_jump_constant_big = 2;
hsp_jump_applied = 0;

//Horizontal key pressed count
hkp_count = 0;
hkp_count_small = 2;
hkp_count_big = 5;

//Init variables
key_left = 0;
key_right = 0;
key_jump = false;

//Sprites
spriteStand = spr_player;
spriteJump = spr_player_jump;
spriteWalk = spr_player_run;

//Health
enemyHealth = 100;
enemyHealth = enemyHealth;
enemy2Health = 100;
enemy2Health = enemy2Health;
